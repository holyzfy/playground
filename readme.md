# 海淀指挥车

## 如何使用

- 请通过[http://localhost](http://localhost)域名访问`index.html`

## 开发文档

### ajax

- 接口地址
访问[http://localhost](http://localhost)视为开发环境，其他域名为生产环境，请补全生产环境的接口地址：`js/common.js`里`urlMap.production`字段

- 数据格式
```js
{
    "status": 0, // 0表示正常响应，其他整数表示异常情况
    "data": {
        // 数据部分放到此字段里，可以是任意的JSON数据类型
    }
}
```
