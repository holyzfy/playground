'use strict';

(function (app) {
var asyncComponent = app.common.asyncComponent;

function getWeather(resolve, reject) {
    var options = {
        name: 'weather',
        template: 'templates/home/weather.html'
    };
    asyncComponent(options, resolve, reject);
}

app.components.getWeather = getWeather;

})(app);
