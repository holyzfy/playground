'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;
var urlMap = common.urlMap;

function getCtrl(resolve, reject) {
    var options = {
        name: 'ctrl',
        template: 'templates/home/ctrl.html',
        data: function () {
            return {
                mediaSelected: false,
                mediaList: [],
                welcome: {
                    content: '',
                    style: {}
                },
                lightSelected: false,
                atmosphereSelected: false
            };
        },
        computed: {
            fontWeight: function () {
                var map = {
                    normal: '正常',
                    bold: '加粗'
                };
                var weight = this.welcome.style['font-weight'];
                return map[weight];
            }
        },
        mixins: [
            common.smoothScroll,
            common.fullscreen
        ],
        created: function () {
            created(this);
        },
        methods: {
            updateBg: function (value) {
                app.instance.bg = value;
            },
            saveWelcome: function () {
                saveWelcome(this);
            },
            requestFullscreen: function (event) {
                var el = $(event.target).closest('.main')[0];
                this.toggleFullScreen(el);
            },
            exitFullscreen: function () {
                var el = $(event.target).closest('.main')[0];
                this.toggleFullScreen(el);
            },
            hideDropdown: function (event) {
                var $ul = $(event.target).closest('ul.ctrl-style-dropdown').css('display', 'none');
                setTimeout(function () {
                    $ul.css('display', '');
                }, 1000);
            },
            setFontFamily: function (value) {
                this.$set(this.welcome.style, 'font-family', value);
            },
            setFontSize: function (value) {
                this.$set(this.welcome.style, 'font-size', value);
            },
            toggleFontWeight: function () {
                var weight = this.welcome.style['font-weight'] || 'normal';
                weight = weight === 'normal' ? 'bold' : 'normal';
                this.$set(this.welcome.style, 'font-weight', weight);
            },
            setColor: function (value) {
                this.$set(this.welcome.style, 'color', value);
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

function created(context) {
    $.getJSON(common.urlMap.welcome, function (result) {
        if(result.status !== 0) {
            return;
        }
        context.welcome = result.data;
    });

    $.getJSON(urlMap.image, function (result) {
        if(result.status !== 0) {
            return;
        }
        context.mediaList = result.data;
        context.updateBg(result.data[0]);
    });
}

function saveWelcome(context) {
    var options = {
        method: 'POST',
        url: common.urlMap.welcome,
        dataType: 'json',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        data: JSON.stringify(context.welcome)
    };
    $.ajax(options).then(function () {
        var value = $.extend(true, {}, context.welcome);
        app.instance.$emit('welcome', value);
        // context.mediaSelected = false;
    });
}

app.components.getCtrl = getCtrl;

})(app);
