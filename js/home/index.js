'use strict';

(function (app) {
var components = app.components;
var common = app.common;
var asyncComponent = common.asyncComponent;

function getHome(resolve, reject) {
    var options = {
        name: 'home',
        template: 'templates/home/index.html',
        data: function () {
            return {
                welcome: {}
            };
        },
        components: {
            weather: components.getWeather,
            ctrl: components.getCtrl
        },
        created: function () {
            created(this);
        }
    };
    asyncComponent(options, resolve, reject);
}

function created(context) {
    $.getJSON(common.urlMap.welcome, function (result) {
        if(result.status !== 0) {
            return;
        }
        context.welcome = result.data;
    });

    app.instance.$on('welcome', function (welcome) {
        context.welcome = welcome;
    });
}

components.getHome = getHome;

})(app);
