'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;
var drag = app.drag;

var list = [];

function getFrame(resolve, reject) {
    var options = {
        name: 'frame',
        template: 'templates/frame.html',
        data: function () {
            return {
                list: list,
                showDownQrcode: {},
                layout: app.instance.layout,
                dragIndex: -1,
                order: {}
            };
        },
        mixins: [
            common.fullscreen
        ],
        created: function () {
            var context = this;
            context.updateList();

            app.instance.$on('layout', function (layout) {
                context.layout = layout;
            });

            if(list.length === 0 || !context.$route.query.url) {
                context.$router.push({name: 'home'});
            }
        },
        watch: {
            $route: function () {
                this.updateList();
            },
            list: function (list) {
                app.instance.frames = list.length;
                var order = this.order;
                list.forEach(function (item, i) {
                    // order[i] = i;
                    Vue.set(order, i, i);
                });

                if(list.length < 1) {
                    this.$router.push({name: 'home'});
                }
            }
        },
        directives: {
            focus: {
                inserted: function (el) {
                    el.focus();
                }
            }
        },
        methods: {
            updateList: function () {
                updateList(this);
            },
            toggleDownQrcode: function (index) {
                toggleDownQrcode(this, index);
            },
            requestFullscreen: function (event) {
                var frame = $(event.target).closest('.frame')[0];
                this.toggleFullScreen(frame);
            },
            exitFullscreen: function () {
                var frame = $(event.target).closest('.frame')[0];
                this.toggleFullScreen(frame);
            },
            close: function (src) {
                var list = this.list;
                var index = list.indexOf(src);
                list.splice(index, 1);
            },
            dragFrame: function (event, index) {
                dragFrame(this, event, index);
            }
        }
    };
    asyncComponent(options, resolve, reject);
}

function updateList(context) {
    var $route = context.$route;
    if($route.name !== 'frame') {
        return;
    }

    var MAX = 4;
    var list = context.list;
    var url = $route.query.url;
    if(list.length < MAX) {
        var existed = list.includes(url);
        url && !existed && list.push(url);
        return;
    }
    var tip = '已经打开了4个子窗口，是否替换第一个窗口？';
    // eslint-disable-next-line no-alert
    confirm(tip) && list.splice(0, 1, url);
}

function toggleDownQrcode(context, index) {
    clearTimeout(toggleDownQrcode.timer[index]);
    toggleDownQrcode.timer[index] = setTimeout(function () {
        var result = !context.showDownQrcode[index];
        context.$set(context.showDownQrcode, index, result);
    }, 200);
}
toggleDownQrcode.timer = {};

function dragFrame(context, event, index) {
    context.dragIndex = index;

    var $siblings = $(context.$el).children('.frame');
    var elementToDrag = event.target;
    var frame = $(elementToDrag).closest('.frame')[0];
    var container = $(frame).closest('.main')[0];
    var $overlay = $(elementToDrag).closest('.frame').find('.frame-overlay');
    var callback = {
        dragmove: function (event, position) {
            $overlay.css({
                'margin-top': position.top,
                'margin-left': position.left
            });

            var overlayRect = $overlay[0].getClientRects()[0];
            $siblings.each(function (i, item) {
                if(i === index) {
                    return;
                }

                var dropRect = item.getClientRects()[0];
                var overlap = isOverlap(overlayRect, dropRect);
                if(overlap) {
                    var order = context.order;
                    var temp = order[index];
                    order[index] = order[i];
                    order[i] = temp;
                    context.dragIndex = -1;
                    return false;
                }
            });
        },
        dragend: function () {
            context.dragIndex = -1;
        }
    };
    drag(elementToDrag, event, callback, container);
}

function isOverlap(dragRect, dropRect) {
    if(!dragRect || !dropRect) {
        return false;
    }
    var top = Math.max(dragRect.top, dropRect.top),
    right = Math.min(dragRect.right, dropRect.right),
    bottom = Math.min(dragRect.bottom, dropRect.bottom),
    left = Math.max(dragRect.left, dropRect.left);
    var throttle = 150;
    return (right - left) > throttle && (bottom - top) > throttle;
}

app.components.getFrame = getFrame;

})(app);
