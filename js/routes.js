'use strict';

(function (app) {

var components = app.components;

app.getRoutes = function () {
    return [
        {
            name: 'home',
            path: '/',
            component: components.getHome
        },
        {
            name: 'frame',
            path: '/frame'
        }
    ];
};

})(app);
