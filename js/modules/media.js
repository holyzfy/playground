'use strict';
(function (app) {

var common = app.common;
var media = {};

media.start = function () {
    new Vue({
        el: '#root',
        data: function () {
            return {
                list: [],
                overlay: {
                    media: null,
                    loopSingle: false,
                    playing: false,
                    volume: 0.5,
                    currentTime: 0,
                    duration: 0,
                    showVolume: false
                },
                btnTimer: null,
                btnStatus: 'show'
            };
        },
        created: function () {
            created(this);
        },
        directives: {
            focus: {
                inserted: function (el) {
                    el.focus();
                }
            }
        },
        methods: {
            volume: function (action) {
                var STEP = 0.1;
                var MIN = 0;
                var MAX = 1;
                var overlay = this.overlay;
                var factor = (action === '-') ? -1 : 1;
                var value = overlay.volume + factor * STEP;
                overlay.volume = Math.min(Math.max(MIN, value), MAX);
            },
            ended: function (item) {
                this.$delete(item, 'status');
            },
            setDuration: function (audio, item) {
                this.$set(item, 'duration', audio.duration.toFixed(0));
            },
            canplay: function () {
                var context = this;
                var media = context.$refs.media;
                context.overlay.duration = media.duration;  
            },
            playNext: function () {
                var context = this;
                var media = context.$refs.media;
                var list = context.list;
                if(context.overlay.loopSingle) {
                    media.play();
                } else {
                    var index = list.map(function (item) {
                        return item.url;
                    }).indexOf(context.overlay.media.url);
                    index = (index + 1) % list.length; 
                    context.overlay.media = list[index];
                }
            },
            playpause: function () {
                var context = this;
                var media = context.$refs.media;
                var action = (media.paused || media.ended) ? 'play' : 'pause';
                media[action]();
            },
            timeupdate: function () {
                var context = this;
                var media = context.$refs.media;
                if(media) {
                    context.overlay.currentTime = media.currentTime;
                }
            },
            updateTime: function (event) {
                if(event.isTrusted) {
                    var context = this;
                    var media = context.$refs.media;
                    media.currentTime = context.overlay.currentTime;
                }
            },
            formatTime: function (second) {
                var hour = Math.floor(second / 3600);
                var minute = Math.floor((second % 3600) / 60);
                var sec = Math.floor(second - hour * 3600 - minute * 60);
                var list = [];
                hour > 0 && list.push(hour);
                list.push(minute, sec);
                return list.map(function (item) {
                    return item < 10 ? ('0' + item) : item;
                }).join(':');
            },
            hideVolumeRange: function () {
                var context = this;
                setTimeout(function () {
                    context.overlay.showVolume = false;
                }, 500);
            },
            updateVolume: function (event) {
                if(event.isTrusted) {
                    var context = this;
                    var media = context.$refs.media;
                    media.volume = context.overlay.volume;
                }
            },
            showBtn: function () {
                clearTimeout(this.btnTimer);
                this.btnStatus = 'show';
            },
            hideBtn: function () {
                var context = this;
                context.btnTimer = setTimeout(function () {
                    context.btnStatus = 'hide';
                }, 3000);
            }
        }
    });
};

function created(context) {
    $.getJSON(common.urlMap.media, function (result) {
        if(result.status !== 0) {
            return;
        }
        context.list = result.data;
    });
}

app.media = media;

})(app);
