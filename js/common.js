'use strict';

var app = {
    components: {}
};

(function (app) {

function getEnv() {
    var env = {
        '127.0.0.1': 'development',
        'localhost': 'development',
        'static.f2e.yiifcms.com': 'development'
    };
    return env[location.hostname] || 'production';
}

var urlMap = {};

urlMap.development = {
    welcome: 'mock/welcome.json',
    image: 'mock/image.json',
    media: 'mock/modules/media.json'
};

// 生产环境的接口地址
urlMap.production = {

};

var smoothScroll = {
    data: function () {
        return {
            overflow: 'hidden',
            scrollTimer: null
        };
    },
    methods: {
        smoothScroll: function () {
            var context = this;
            context.overflow = 'auto';
            clearTimeout(context.scrollTimer);
            context.scrollTimer = setTimeout(function () {
                context.overflow = 'hidden';
            }, 100);
        }
    }
};

var hover = {
    data: function () {
        return {
            hover: false,
            hoverTimer: null
        };
    },
    methods: {
        hoverShow: function () {
            clearTimeout(this.hoverTimer);
            this.hover = true;
        },
        hoverHide: function () {
            var context = this;
            context.hoverTimer = setTimeout(function () {
                context.hover = false;
            }, 300);
        }
    }
};

var fullscreen = {
    computed: {
        fullscreenElementName: function () {
            return getSuppportFnName([
                'fullscreenElement',
                'webkitFullscreenElement',
                'mozFullScreenElement'
            ]);
        },
        requestFullscreenName: function () {
            return getSuppportFnName([
                'requestFullscreen',
                'webkitRequestFullscreen',
                'mozRequestFullScreen'
            ]);
        },
        onfullscreenchangeName: function () {
            return getSuppportFnName([
                'onfullscreenchange',
                'onwebkitfullscreenchange',
                'onmozfullscreenchange'
            ]);
        },
        exitFullscreenName: function () {
            return getSuppportFnName([
                'exitFullscreen',
                'webkitExitFullscreen',
                'mozCancelFullScreen'
            ]);
        }
    },
    methods: {
        toggleFullScreen: function (el) {
            if (!document[this.fullscreenElementName]) {
                el[this.requestFullscreenName]();
            } else {
                if (document[this.exitFullscreenName]) {
                    document[this.exitFullscreenName](); 
                }
            }
        },
    }
};

function getSuppportFnName(list) {
    var undef;
    for(var index in list) {
        var name = list[index];
        var fn = document[name] !== undef ? document[name] : document.documentElement[name];
        if(fn !== undef) {
            return name;
        }
    }
}

function asyncComponent(options, resolve, reject) {
    var url = options.template;
    asyncComponent.cache = asyncComponent.cache || {};
    
    var promise;
    if(asyncComponent.cache[url]) {
        promise = $.Deferred().resolve(asyncComponent.cache[url]).promise();
    } else {
        promise = $.get(url);
    }

    promise.then(function (html) {
        asyncComponent.cache[url] = html;
        var component = $.extend({}, options, {template: html});
        resolve(component);
    }).fail(function (xhr, status, error) {
        reject(error);
    });
}

if(getEnv() === 'production') {
    Vue.config.silent = true;
    Vue.config.productionTip = false;
    Vue.config.errorHandler = $.noop;
}

app.common = {
    urlMap: urlMap[getEnv()],
    smoothScroll: smoothScroll,
    hover: hover,
    fullscreen: fullscreen,
    asyncComponent: asyncComponent
};

})(app);
