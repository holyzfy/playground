'use strict';

(function (app) {

var common = app.common;
var asyncComponent = common.asyncComponent;

function getNav(resolve, reject) {
    var options = {
        name: 'aside-nav',
        template: 'templates/index/nav.html',
        data: function () {
            return {
                path: '/'
            };
        },
        computed: {
            selected: function () {
                return this.$route.path === '/';
            }
        },
        watch: {
            '$route.path': function (value) {
                this.path = (value === '/') ? '/frame' : '/';
            } 
        },
        mixins: [common.smoothScroll]
    };
    asyncComponent(options, resolve, reject);
}

app.components.getNav = getNav;

})(app);
