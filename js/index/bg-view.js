'use strict';

(function (app) {
var asyncComponent = app.common.asyncComponent;

function getBgView(resolve, reject) {
    var options = {
        name: 'bg-view',
        template: 'templates/index/bg-view.html',
        props: ['bg']
    };
    asyncComponent(options, resolve, reject);
}

app.components.getBgView = getBgView;

})(app);
