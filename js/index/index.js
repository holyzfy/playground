'use strict';

(function (app) {

var components = app.components;

function start() {
    app.instance = new Vue({
        el: '#app',
        router: new VueRouter({
            routes: app.getRoutes()
        }),
        data: {
            bg: {},
            layout: '3-left',
            frames: 0
        },
        computed: {
            isFrame: function () {
                return this.$route.name === 'frame';
            }
        },
        components: {
            'aside-nav': components.getNav,
            'bg-view': components.getBgView,
            frame: components.getFrame
        },
        watch: {
            layout: function (value) {
                this.$emit('layout', value);
            }
        }
    });
}

app.start = start;

})(app);
